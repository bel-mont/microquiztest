﻿using Moq;
using FluentAssertions;
using NUnit.Framework;
using System.Web.Mvc;
using MicroquizTest.Controllers;
using MicroquizTest.Models;
using System.Collections.Generic;
using MicroquizTest.Common.Models;
using static MicroquizTest.Common.Constants;
using MicroquizTest.Common.Interfaces;
using MicroquizTest.App_Start;
using System;

namespace MicroquizTest.Tests.Controllers
{
    /// <summary>
    /// Tests each unit of PollController.
    /// </summary>
    [TestFixture]
    public class PollControllerTest
    {
        #region Members
        private PollController _controller;
        private Mock<IRepository<QuestionType>> _questionTypeRepository;
        private List<QuestionType> _questionTypesFixture;
        private Mock<IEditableRepository<Poll>> _pollRepository;
        #endregion Members

        #region Methods

        /// <summary>
        /// Sets up the fixtures and mocks required for the tests.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            #region Question Types Fixture
            _questionTypesFixture = new List<QuestionType>
            {
                new QuestionType { ID = QuestionTypeIdentifier.MultipleChoiceSingleAnswer, Title = "Multiple Choice with Single Answer" },
                new QuestionType { ID = QuestionTypeIdentifier.MultipleChoiceMultipleAnswers, Title = "Multiple Choice with Multiple Answers" }
            };
            #endregion Question Types Fixture

            #region Mocks and Setups
            _questionTypeRepository = new Mock<IRepository<QuestionType>>();
            _questionTypeRepository.Setup(r => r.Get()).Returns(_questionTypesFixture);
            _pollRepository = new Mock<IEditableRepository<Poll>>();
            _pollRepository.Setup(r => r.Insert(It.IsAny<Poll>())).Returns(new Poll());
            AutomapperConfig.RegisterMappings();
            #endregion Mocks and Setups

            _controller = new PollController(_questionTypeRepository.Object, _pollRepository.Object);
        }
        

        /// <summary>
        /// The Create Action must return a view, always.
        /// </summary>
        [Test]
        public void PollController_Create_ReturnsView()
        {
            // Arrange

            // Act
            ActionResult result = _controller.Create();

            // Assert
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        /// <summary>
        /// The Create Action always contains the specified Model.
        /// </summary>
        [Test]
        public void PollController_Create_ViewContainsPollCreateFormViewModel()
        {
            // Arrange

            // Act
            ViewResult result = _controller.Create() as ViewResult;
            
            // Assert
            Assert.IsInstanceOf(typeof(PollFormViewModel), result.Model);
        }

        /// <summary>
        /// The ViewModel's properties are set correctly, fulfilling the acceptance criteria for the new poll form.
        /// </summary>
        [Test]
        public void PollController_InitializeEmptyViewModel()
        {
            // Arrange
            var expectedResult = new PollFormViewModel
            {
                IncludeDemographicData = true,
                Choices = new List<ChoiceFormViewModel> { new ChoiceFormViewModel { Arrangement = 1 }, new ChoiceFormViewModel { Arrangement = 2 } },
                QuestionTypes = _questionTypesFixture
            };

            // Act
            PollFormViewModel result = _controller.GetEmptyViewModel();

            // Assert
            result.ShouldBeEquivalentTo(expectedResult);
        }

        /// <summary>
        /// After creating a new poll successfuly, the action should redirect to the Respond action.
        /// </summary>
        [Test]
        public void PollController_Create_IsSuccessful_Redirects()
        {
            // Arrange
            var form = new PollFormViewModel();

            // Act
            RedirectResult result = _controller.Create(form) as RedirectResult;

            // Assert
            Assert.AreEqual("Respond", result.Url);
        }

        /// <summary>
        /// If an exception is thrown while creating a Poll, the Create view is returned again.
        /// </summary>
        [Test]
        public void PollControlloer_Create_Fails_ReturnsView()
        {
            // Arrange
            var form = new PollFormViewModel();
            _pollRepository.Setup(c => c.Insert(It.IsAny<Poll>())).Throws(new Exception("Mock exception."));

            // Act
            ViewResult result = _controller.Create(form) as ViewResult;

            // Assert
            Assert.AreEqual(string.Empty, result.ViewName);
        }
        #endregion Methods
    }
}
