﻿using System.ComponentModel.DataAnnotations;

namespace MicroquizTest.Models
{
    public class ChoiceFormViewModel
    {
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }
        public short Arrangement { get; set; }
    }
}