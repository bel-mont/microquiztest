﻿using MicroquizTest.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static MicroquizTest.Common.Constants;

namespace MicroquizTest.Models
{
    public class PollFormViewModel
    {
        public PollFormViewModel()
        {
            Choices = new List<ChoiceFormViewModel>();
            QuestionTypes = new List<QuestionType>();
        }

        [Required]
        [MaxLength(350)]
        public string Title { get; set; }
        public DuplicateResponseCheckType? DuplicateResponseCheck { get; set; }
        public bool IncludeDemographicData { get; set; }
        public QuestionTypeIdentifier QuestionType { get; set; }
        public List<ChoiceFormViewModel> Choices { get; set; }
        public List<QuestionType> QuestionTypes { get; set; }
    }
}