﻿using AutoMapper;
using MicroquizTest.Common.Interfaces;
using MicroquizTest.Common.Models;
using MicroquizTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MicroquizTest.Controllers
{
    /// <summary>
    /// Serves for CRUD operations for the Polls.
    /// </summary>
    public class PollController : Controller
    {
        #region Members
        private readonly IRepository<QuestionType> _questionTypeRepository;
        private readonly IEditableRepository<Poll> _pollRepository;
        #endregion Members

        public PollController(IRepository<QuestionType> questionTypeRepository, IEditableRepository<Poll> pollRepository)
        {
            _questionTypeRepository = questionTypeRepository;
            _pollRepository = pollRepository;
        }
        
        #region Methods

        #region Actions

        /// <summary>
        /// Presents the initial form to create a new poll.
        /// </summary>
        /// <returns>A ResultView with a ViewModel used when creating a new poll.</returns>
        public ActionResult Create()
        {
            return View(GetEmptyViewModel());
        }

        /// <summary>
        /// Attempts to create a new Poll and redirects to the Respond Action if successful. Otherwise, the View is returned.
        /// </summary>
        /// <param name="form">ViewModel with the data of the new Poll.</param>
        /// <returns>ActionResult containing either the RedirectResult or ViewResult.</returns>
        [HttpPost]
        public ActionResult Create(PollFormViewModel form)
        {
            try
            { 
                Poll newPoll = Mapper.Map<PollFormViewModel, Poll>(form);
                _pollRepository.Insert(newPoll);
                return Redirect("Respond");
            }
            catch (Exception ex)
            {
                // Add error logging.
                Console.WriteLine(ex.Message);
                form.QuestionTypes = _questionTypeRepository.Get().ToList();
                return View(form);
            }
        }

        #endregion Actions

        #region Helpers

        /// <summary>
        /// Creates and initializes a new empty ViewModel, used when creating new polls.
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public PollFormViewModel GetEmptyViewModel()
        {
            var viewModel = new PollFormViewModel
            {
                IncludeDemographicData = true,
                QuestionTypes = _questionTypeRepository.Get().ToList()
            };
            // A minimum of 2 choices are shown by default on the form.
            viewModel.Choices.Add(new ChoiceFormViewModel { Arrangement = 1 });
            viewModel.Choices.Add(new ChoiceFormViewModel { Arrangement = 2 });
            return viewModel;
        }
        
        #endregion #Helpers

        #endregion Methods

    }
}