﻿using AutoMapper;
using MicroquizTest.Common.Models;
using MicroquizTest.Models;
using System.Linq;

namespace MicroquizTest.App_Start
{
    public class AutomapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<PollFormViewModel, Poll>()
                    .ForMember(d => d.Question, o => o.MapFrom(s => new Question
                    {
                        Title = s.Title,
                        QuestionTypeID = s.QuestionType,
                        Choices = s.Choices.Select(c => new Choice { Title = c.Title, Arrangement = c.Arrangement }).ToList()
                    }))
                    .ForMember(d => d.PollSettings, o => o.MapFrom(s => new PollSettings
                    {
                        IncludeDemographicData = s.IncludeDemographicData,
                        DuplicateResponseCheck = s.DuplicateResponseCheck
                    }));
            });
        }
    }
}