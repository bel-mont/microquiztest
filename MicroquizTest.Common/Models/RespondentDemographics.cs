﻿namespace MicroquizTest.Common.Models
{
    public class RespondentDemographics
    {
        public int RespondentID { get; set; }
        public string IP { get; set; }
        public string Country { get; set; }
        public string Browser { get; set; }
        public string BrowserVersionSimplified { get; set; }
        public string BrowserVersion { get; set; }
        public string Platform { get; set; }
        public string PlatformVersionSimplified { get; set; }
        public string PlatformVersion { get; set; }

        public virtual Respondent Respondent { get; set; }
    }
}