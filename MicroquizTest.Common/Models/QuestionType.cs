﻿using static MicroquizTest.Common.Constants;

namespace MicroquizTest.Common.Models
{
    public class QuestionType
    {
        public QuestionTypeIdentifier ID { get; set; }
        public string Title { get; set; }
    }
}