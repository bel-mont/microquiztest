﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static MicroquizTest.Common.Constants;

namespace MicroquizTest.Common.Models
{
    public class Question
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public QuestionTypeIdentifier QuestionTypeID { get; set; }

        public virtual QuestionType QuestionType { get; set; }
        public virtual Poll Poll { get; set; }
        public virtual ICollection<Choice> Choices { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}