﻿using System.Collections.Generic;

namespace MicroquizTest.Common.Models
{
    public class Poll
    {
        public int ID { get; set; }
        public int CreationTimestamp { get; set; }
        public string IP { get; set; }

        public virtual PollSettings PollSettings { get; set; }
        public virtual Question Question { get; set; }
        public virtual ICollection<Respondent> Respondents { get; set; }
    }
}
