﻿using System.Collections.Generic;

namespace MicroquizTest.Common.Models
{
    public class Choice
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public short Arrangement { get; set; }
        public int QuestionID { get; set; }

        public virtual Question Question { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}