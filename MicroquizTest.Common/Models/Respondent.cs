﻿namespace MicroquizTest.Common.Models
{
    public class Respondent
    {
        public int ID { get; set; }
        public int PollID { get; set; }
        public int CreationTimestamp { get; set; }

        public virtual Poll Poll { get; set; }
        public virtual RespondentDemographics RespondentDemographics { get; set; }
    }
}