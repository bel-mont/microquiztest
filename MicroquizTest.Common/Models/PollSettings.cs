﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static MicroquizTest.Common.Constants;

namespace MicroquizTest.Common.Models
{
    public class PollSettings
    {
        public int PollID { get; set; }
        public DuplicateResponseCheckType? DuplicateResponseCheck { get; set; }
        public bool IncludeDemographicData { get; set; }
        
        public virtual Poll Poll { get; set; }
    }
}