﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroquizTest.Common.Interfaces
{
    public interface IRepository<TEntity>
    {
        ICollection<TEntity> Get();
        ICollection<TEntity> Find(dynamic searchFilter);
        TEntity Get(int id);
    }
}
