﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroquizTest.Common.Interfaces
{
    public interface IEditableRepository<T> : IRepository<T>
    {
        T Insert(T entity);
        T Update(T entity);
        void Delete(T entity);
    }
}
