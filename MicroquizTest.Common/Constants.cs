﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroquizTest.Common
{
    public static class Constants
    {
        public enum DuplicateResponseCheckType
        {
            Browser = 0,
            IP = 1
        };

        public enum QuestionTypeIdentifier
        {
            MultipleChoiceSingleAnswer = 1,
            MultipleChoiceMultipleAnswers = 2
        };

        public static string MicroquizDBConnectionStringName { get { return "MicroquizDB"; } }
    }
}
