﻿using MicroquizTest.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroquizTest.Common.BaseRepositories
{
    /// <summary>
    /// Allows full CRUD operations on the specified entities.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BaseEditableRepository<TEntity> : BaseRepository<TEntity>, IEditableRepository<TEntity>
        where TEntity : class
    {
        #region Constructors

        /// <summary>
        /// Creates a new Repository with the specified dependencies.
        /// </summary>
        /// <param name="context">Context to use when interacting with the database layer.</param>
        public BaseEditableRepository(DbContext context) : base(context)
        {
        }

        #endregion Constructaors

        #region Methods

        /// <summary>
        /// Deletes an entity from the context.
        /// </summary>
        /// <param name="entity">Entity to delete.</param>
        public virtual void Delete(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        /// <summary>
        /// Inserts a new entity into the context.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        /// <returns>The inserted entity.</returns>
        public virtual TEntity Insert(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        /// <summary>
        /// Updates an entity with new information.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        /// <returns>The updated entity.</returns>
        public virtual TEntity Update(TEntity entity)
        {
            _context.SaveChanges();
            return entity;
        }

        #endregion Methods
    }
}
