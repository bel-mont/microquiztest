﻿using MicroquizTest.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroquizTest.Common.BaseRepositories
{
    /// <summary>
    /// Allows read operations to be performed on the specified entity.
    /// </summary>
    /// <typeparam name="TEntity">Entity to operate on.</typeparam>
    public class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        #region Members

        protected readonly DbContext _context;
        
        #endregion Members

        #region Constructors

        /// <summary>
        /// Creates a new Repository with the specified dependencies.
        /// </summary>
        /// <param name="context">Context to use when interacting with the database layer.</param>
        public BaseRepository(DbContext context)
        {
            _context = context;
        }
        
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Finds entities that match the specified filter.
        /// </summary>
        /// <param name="searchFilter">Contains the filters to use in the query.</param>
        /// <returns>Entities that matched the provided filters.</returns>
        public virtual ICollection<TEntity> Find(dynamic searchFilter)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all available entities.
        /// </summary>
        /// <returns>Retrieved entities.</returns>
        public virtual ICollection<TEntity> Get()
        {
            ICollection<TEntity> entities = _context.Set<TEntity>().ToList();
            return entities;
        }

        /// <summary>
        /// Gets the entity that has the specified ID.
        /// </summary>
        /// <param name="id">ID of the entity to retrieve.</param>
        /// <returns>The retrieved entity.</returns>
        public virtual TEntity Get(int id)
        {
            TEntity entity = _context.Set<TEntity>().Find(id);
            return entity;
        }

        #endregion Methods
    }
}
