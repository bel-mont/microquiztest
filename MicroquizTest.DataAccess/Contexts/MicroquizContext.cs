﻿using MicroquizTest.Common;
using MicroquizTest.Common.Models;
using MicroquizTest.DataAccess.ModelMappings;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MicroquizTest.DataAccess.Contexts
{
    public class MicroquizContext : DbContext
    {
        #region Members

        public DbSet<Poll> Polls { get; set; }
        public DbSet<PollSettings> PollSettings { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Choice> Choices { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<Respondent> Respondents { get; set; }
        public DbSet<RespondentDemographics> RespondentDemographics { get; set; }
        public DbSet<Answer> Answers { get; set; }

        #endregion Members

        #region Constructors

        public MicroquizContext() : base(Constants.MicroquizDBConnectionStringName)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MicroquizContext>());
        }

        #endregion Constructors

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PollConfiguration());
            modelBuilder.Configurations.Add(new PollSettingsConfiguration());
            modelBuilder.Configurations.Add(new RespondentConfiguration());
            modelBuilder.Configurations.Add(new RespondentDemographicsConfiguration());
            modelBuilder.Configurations.Add(new AnswerConfiguration());
            modelBuilder.Configurations.Add(new ChoiceConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new QuestionTypeConfiguration());

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        #endregion Methods
    }
}
