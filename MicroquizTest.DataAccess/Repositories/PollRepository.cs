﻿using MicroquizTest.Common.BaseRepositories;
using MicroquizTest.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MicroquizTest.DataAccess.Contexts;

namespace MicroquizTest.DataAccess.Repositories
{
    public class PollRepository : BaseEditableRepository<Poll>
    {
        public PollRepository(MicroquizContext context) : base(context)
        {
        }
    }
}
