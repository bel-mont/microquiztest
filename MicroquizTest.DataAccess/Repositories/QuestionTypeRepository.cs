﻿using MicroquizTest.Common.BaseRepositories;
using MicroquizTest.Common.Models;
using MicroquizTest.DataAccess.Contexts;

namespace MicroquizTest.DataAccess.Repositories
{
    public class QuestionTypeRepository : BaseRepository<QuestionType>
    {
        #region Constructors
        public QuestionTypeRepository(MicroquizContext context) : base(context)
        {
        }
        #endregion Constructors
    }
}
