namespace MicroquizTest.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RespondentID = c.Int(nullable: false),
                        QuestionID = c.Int(nullable: false),
                        ChoiceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Choice", t => t.ChoiceID, cascadeDelete: true)
                .ForeignKey("dbo.Question", t => t.QuestionID, cascadeDelete: true)
                .ForeignKey("dbo.Respondent", t => t.RespondentID, cascadeDelete: true)
                .Index(t => t.RespondentID)
                .Index(t => t.QuestionID)
                .Index(t => t.ChoiceID);
            
            CreateTable(
                "dbo.Choice",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 200),
                        Arrangement = c.Short(nullable: false),
                        QuestionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Question", t => t.QuestionID, cascadeDelete: false)
                .Index(t => t.QuestionID);
            
            CreateTable(
                "dbo.Question",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 350),
                        QuestionTypeID = c.Int(nullable: false),
                        PollID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Poll", t => t.PollID)
                .ForeignKey("dbo.QuestionType", t => t.QuestionTypeID, cascadeDelete: true)
                .Index(t => t.QuestionTypeID)
                .Index(t => t.PollID);
            
            CreateTable(
                "dbo.Poll",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CreationTimestamp = c.Int(nullable: false),
                        IP = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PollSettings",
                c => new
                    {
                        PollID = c.Int(nullable: false),
                        DuplicateResponseCheck = c.Int(),
                        IncludeDemographicData = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PollID)
                .ForeignKey("dbo.Poll", t => t.PollID)
                .Index(t => t.PollID);
            
            CreateTable(
                "dbo.Respondent",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        PollID = c.Int(nullable: false),
                        CreationTimestamp = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Poll", t => t.PollID, cascadeDelete: true)
                .ForeignKey("dbo.RespondentDemographics", t => t.ID)
                .Index(t => t.ID)
                .Index(t => t.PollID);
            
            CreateTable(
                "dbo.RespondentDemographics",
                c => new
                    {
                        RespondentID = c.Int(nullable: false, identity: true),
                        IP = c.String(),
                        Country = c.String(),
                        Browser = c.String(),
                        BrowserVersionSimplified = c.String(),
                        BrowserVersion = c.String(),
                        Platform = c.String(),
                        PlatformVersionSimplified = c.String(),
                        PlatformVersion = c.String(),
                    })
                .PrimaryKey(t => t.RespondentID);
            
            CreateTable(
                "dbo.QuestionType",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Title = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Answer", "RespondentID", "dbo.Respondent");
            DropForeignKey("dbo.Question", "QuestionTypeID", "dbo.QuestionType");
            DropForeignKey("dbo.Respondent", "ID", "dbo.RespondentDemographics");
            DropForeignKey("dbo.Respondent", "PollID", "dbo.Poll");
            DropForeignKey("dbo.Question", "PollID", "dbo.Poll");
            DropForeignKey("dbo.PollSettings", "PollID", "dbo.Poll");
            DropForeignKey("dbo.Choice", "QuestionID", "dbo.Question");
            DropForeignKey("dbo.Answer", "QuestionID", "dbo.Question");
            DropForeignKey("dbo.Answer", "ChoiceID", "dbo.Choice");
            DropIndex("dbo.Respondent", new[] { "PollID" });
            DropIndex("dbo.Respondent", new[] { "ID" });
            DropIndex("dbo.PollSettings", new[] { "PollID" });
            DropIndex("dbo.Question", new[] { "PollID" });
            DropIndex("dbo.Question", new[] { "QuestionTypeID" });
            DropIndex("dbo.Choice", new[] { "QuestionID" });
            DropIndex("dbo.Answer", new[] { "ChoiceID" });
            DropIndex("dbo.Answer", new[] { "QuestionID" });
            DropIndex("dbo.Answer", new[] { "RespondentID" });
            DropTable("dbo.QuestionType");
            DropTable("dbo.RespondentDemographics");
            DropTable("dbo.Respondent");
            DropTable("dbo.PollSettings");
            DropTable("dbo.Poll");
            DropTable("dbo.Question");
            DropTable("dbo.Choice");
            DropTable("dbo.Answer");
        }
    }
}
