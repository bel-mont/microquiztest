namespace MicroquizTest.DataAccess.Migrations
{
    using Common.Models;
    using Contexts;
    using System.Data.Entity.Migrations;
    using static Common.Constants;
    internal sealed class Configuration : DbMigrationsConfiguration<MicroquizContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MicroquizContext context)
        {
            context.QuestionTypes.AddOrUpdate(
              q => q.ID,
              new QuestionType { ID = QuestionTypeIdentifier.MultipleChoiceSingleAnswer, Title = "Multiple Choice with Single Answer" },
              new QuestionType { ID = QuestionTypeIdentifier.MultipleChoiceMultipleAnswers, Title = "Multiple Choice with Multiple Answers" }
            );
        }
    }
}
