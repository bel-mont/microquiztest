﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class QuestionTypeConfiguration : EntityTypeConfiguration<QuestionType>
    {
        public QuestionTypeConfiguration()
        {
            ToTable("QuestionType");
            Property(s => s.Title).HasMaxLength(150);
        }
    }
}
