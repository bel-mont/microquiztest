﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class RespondentConfiguration : EntityTypeConfiguration<Respondent>
    {
        public RespondentConfiguration()
        {
            ToTable("Respondent");
        }
    }
}
