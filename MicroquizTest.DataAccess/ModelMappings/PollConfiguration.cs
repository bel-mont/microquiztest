﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class PollConfiguration : EntityTypeConfiguration<Poll>
    {
        public PollConfiguration()
        {
            ToTable("Poll");
            HasKey(s => s.ID);
            Property(s => s.IP).HasMaxLength(100);
            HasOptional(s => s.Question).WithOptionalPrincipal(q => q.Poll).Map(s => s.MapKey("PollID"));
        }
    }
}
