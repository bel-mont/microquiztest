﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class PollSettingsConfiguration : EntityTypeConfiguration<PollSettings>
    {
        public PollSettingsConfiguration()
        {
            ToTable("PollSettings");
            HasKey(s => s.PollID);
            HasRequired(s => s.Poll).WithRequiredDependent(s => s.PollSettings);
        }
    }
}
