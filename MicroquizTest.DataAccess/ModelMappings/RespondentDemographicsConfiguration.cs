﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class RespondentDemographicsConfiguration : EntityTypeConfiguration<RespondentDemographics>
    {
        public RespondentDemographicsConfiguration()
        {
            ToTable("RespondentDemographics");
            HasKey(s => s.RespondentID);
            HasRequired(s => s.Respondent).WithRequiredPrincipal(s => s.RespondentDemographics);
        }
    }
}
