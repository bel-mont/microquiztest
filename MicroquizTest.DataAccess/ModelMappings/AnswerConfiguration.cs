﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class AnswerConfiguration : EntityTypeConfiguration<Answer>
    {
        public AnswerConfiguration()
        {
            ToTable("Answer");
        }
    }
}
