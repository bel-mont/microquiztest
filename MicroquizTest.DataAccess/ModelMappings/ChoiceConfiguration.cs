﻿using MicroquizTest.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace MicroquizTest.DataAccess.ModelMappings
{
    public class ChoiceConfiguration : EntityTypeConfiguration<Choice>
    {
        public ChoiceConfiguration()
        {
            ToTable("Choice");
            Property(s => s.Title).HasMaxLength(200);
        }
    }
}
